<!DOCTYPE html>
<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

	<title>Admin</title>
	<style type="text/css">
		body 
		{
		  background-color: #222222;
		  background: linear-gradient(135deg, #666464 25%, transparent 25%) -50px 0,
			    linear-gradient(225deg, #666464 25%, transparent 25%) -50px 0,
			    linear-gradient(315deg, #666464 25%, transparent 25%),
			    linear-gradient(45deg, #666464 25%, transparent 25%);
			    background-size: 100px 100px;
			    background-color: #222222;
		}
		.container
		{
			background-color: white;
			margin-left: 10%;
			margin-right: 10%;
			margin-top: 10px;
			height: 100%;
			width: 80%;
			border: 3px solid gray;
			border-radius: 25px;
		}
		.header
		{
			text-align: center;
			position: center;
		}
		.header h2
		{
			font-size: 40px;
			background-color: #2962FF;
			color: white;
			margin-left: 10%;
			margin-right: 10%;
			border-radius: 40px;
		}
		.table
		{
			overflow-x: auto;
			margin-left: 1%;
			margin-right: 1%;
			margin-top: 20px;
			margin-bottom: 20px;
		}
		table 
		{
		  border-collapse: collapse;
		  width: 100%;
		}
		th 
		{
		  background-color: #4CAF50;
		  color: white;
		}
		th, td 
		{
		  text-align: center;
		  padding: 8px;
		}
		.addbutton
		{
			text-decoration: none;
			background-color: #4CAF50;
			border-radius: 25px;
			border: 1px solid gray;
			color: white;
			padding: 5px;
			align: right;
			margin: 10px 20px 10px 20px;
		}
		/* The Modal (background) */
.editmodal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.editmodal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  border: 1px solid #888;
  width: 100%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
  from {top:-300px; opacity:0} 
  to {top:0; opacity:1}
}

@keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

/* The Close Button */
.editclose {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.editclose:hover,
.editclose:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.editmodal.open {
   display: flex;
   flex-direction: column;
}
.editmodal-header {
  padding: 2px 16px;
  background-color: #5cb85c;
  color: white;
}

.editmodal-body {padding: 2px 16px;}

.editmodal-footer {
  padding: 2px 16px;
  background-color: #5cb85c;
  color: white;
}
.deletemodal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.deletemodal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  border: 1px solid #888;
  width: 100%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
  from {top:-300px; opacity:0} 
  to {top:0; opacity:1}
}

@keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

/* The Close Button */
.deleteclose {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.deleteclose:hover,
.deleteclose:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.deletemodal.open {
   display: flex;
   flex-direction: column;
}
.deletemodal-header {
  padding: 2px 16px;
  background-color: #5cb85c;
  color: white;
}

.deletemodal-body {padding: 2px 16px;}

.deletemodal-footer {
  padding: 2px 16px;
  background-color: #5cb85c;
  color: white;
}
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  border: 1px solid #888;
  width: 100%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
  from {top:-300px; opacity:0} 
  to {top:0; opacity:1}
}

@keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

/* The Close Button */
.close {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.modal.open {
   display: flex;
   flex-direction: column;
}
.modal-header {
  padding: 2px 16px;
  background-color: #5cb85c;
  color: white;
}

.modal-body {padding: 2px 16px;}

.modal-footer {
  padding: 2px 16px;
  background-color: #5cb85c;
  color: white;
}
input[type=text] {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 100%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=date]{
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit]:hover {
  background-color: #45a049;
}
.editbtn
{
	background-color: blue;
	border-radius: 5px;
	border: 1px solid gray;
	color: white;
	width: 80%;
}
.deletebtn
{
	background-color: red;
	border-radius: 5px;
	border: 1px solid gray;
	color: white;
	width: 80%;
}
	</style>
}
</head>
<body>
	<div class="container">
		<div class="header">
			<h2>Orders</h2>
		</div>
		<a href="#" class="addbutton" data-toggle="modal" data-target="#addordermodal" onclick="addorder()" id="addorderbtn"> Add Order</a>
		<div class="table">
			<table cellspacing="0" width="100%" id="ordertable">
				<thead>
				<tr>
					<th>Name</th>
					<th>Date</th>
					<th>Bread</th>
					<th>Sauce</th>
					<th>Sandwich</th>
					<th>Cheese</th>
					<th>Vegies</th>
					<th>Option</th>
				</tr>	
				</thead>
				
			</table>
		</div>
	</div>


<!-- ADD ORDER MODAL -->
<div id="addordermodal" class="modal">
	<div class="modal-header">
    <span class="close">&times;</span>
    	<h2>Pre-Order your SUB by 8:30am</h2>
  	</div>
  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p class="para" style="position: center; text-align: center; font-size: 30px">Late orders are not accepted</p>
    <p class="para" style="position: center; text-align: center; font-size: 20px; color: red;"><u>Orders not picked-up will not be allowed to pre-order again</u><br><img src="../images/sandwich.png" style="width: 100px; height: 50px;"></p>
    <div style="margin-left: 20%; margin-right: 20%;padding-bottom: 50px; align:center">

    <form method="POST" action="addorder.php" id="addorderform">
    <label>First and Last Name: </label><input type="text" name="ordername" id="ordername" required=""><label class="errorordername" id="errorordername"></label>
    <label>Date: </label><input type="date" name="orderdate" id="orderdate" required="">
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#1 Bread</label><br>
    		<input type="radio" name="orderbread" id="orderbread" value="Whole Wheat" checked="Checked">Whole Wheat
    		<input type="radio" name="orderbread" id="orderbread" value="Italian Herb">Italian Herb
    		<input type="radio" name="orderbread" id="orderbread" value="Jalapeno Parmesan">Jalapeno Parmesan
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#2 Sauce</label><br>
    		<input type="radio" name="ordersauce" id="ordersauce" value="Mayo" checked="Checked">Mayo
    		<input type="radio" name="ordersauce" id="ordersauce" value="Mustard">Mustard
    		<input type="radio" name="ordersauce" id="ordersauce" value="Honey Mustard">Honey Mustard
    		<input type="radio" name="ordersauce" id="ordersauce" value="Spicy Mayo">Spicy Mayo
    		<input type="radio" name="ordersauce" id="ordersauce" value="No Sauce">No Sauce
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#3 Sandwich Type</label><br>
    		<input type="radio" name="ordersandwich" id="ordersandwich" value="Turkey Bacon Club" checked="Checked">Turkey Bacon Club
    		<input type="radio" name="ordersandwich" id="ordersandwich" value="Oven Roasted Turkey">Oven Roasted Turkey
    		<input type="radio" name="ordersandwich" id="ordersandwich" value="Savory Ham">Savory Ham
    		<input type="radio" name="ordersandwich" id="ordersandwich" value="Italian">Italian(Salami,Ham & Pepperoni)
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#4 Cheese</label><br>
    		<input type="radio" name="ordercheese" id="ordercheese" value="American" checked="Checked">American
    		<input type="radio" name="ordercheese" id="ordercheese" value="Swiss">Swiss
    		<input type="radio" name="ordercheese" id="ordercheese" value="Pepper Jack">Pepper Jack
    		<input type="radio" name="ordercheese" id="ordercheese" value="No Cheese">No Cheese
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#5 Veggies</label><br>
    		<input type="radio" name="orderveggies" id="orderveggies" value="Cucumber" checked="Checked">Cucumber
    		<input type="radio" name="orderveggies" id="orderveggies" value="Lettuce">Lettuce
    		<input type="radio" name="orderveggies" id="orderveggies" value="Peppers-Banana">Peppers-Banana
    		<input type="radio" name="orderveggies" id="orderveggies" value="Peppers-Jalapeno">Peppers-Jalapeno
    		<input type="radio" name="orderveggies" id="orderveggies" value="Peppers-Green and Red">Peppers-Green and Red
    		<br>
    		<input type="radio" name="orderveggies" id="orderveggies" value="Pickles">Pickles
    		<input type="radio" name="orderveggies" id="orderveggies" value="Spinach">Spinach
    		<input type="radio" name="orderveggies" id="orderveggies" value="Tomato">Tomato
    		<input type="radio" name="orderveggies" id="orderveggies" value="Olives">Olives
    		<input type="radio" name="orderveggies" id="orderveggies" value="Onions">Onions
    		<input type="radio" name="orderveggies" id="orderveggies" value="No Veggies">No Veggies
    	</div>
    	<p style="font-size: 20px;"><u>Order for 8:30am</u></p>
    	<input type="submit" name="submit" id="submit" onclick="order()" value="Add Order">
    	</form>
	</div>
  </div>
	<div class="modal-footer">
    <h3>Modal Footer</h3>
  	</div>
</div>
<!-- ADD ORDER MODAL -->

<!-- EDIT ORDER MODAL -->
<div id="editmodal" class="editmodal">
	<div class="editmodal-header">
    <span class="editclose">&times;</span>
    	<h2>Edit Order</h2>
  	</div>
  <!-- Modal content -->
  <div class="editmodal-content">
    <span class="editclose">&times;</span>
    <p class="para" style="position: center; text-align: center; font-size: 30px">Late orders are not accepted</p>
    <p class="para" style="position: center; text-align: center; font-size: 20px; color: red;"><u>Orders not picked-up will not be allowed to pre-order again</u><br><img src="../images/sandwich.png" style="width: 100px; height: 50px;"></p>
    <div style="margin-left: 20%; margin-right: 20%;padding-bottom: 50px; align:center">

    <form method="POST" action="orderedit.php" id="editorderform">
    <label>First and Last Name: </label><input type="text" name="editordername" id="editordername" required=""><label class="errorordername" id="errorordername"></label>
    <label>Date: </label><input type="date" name="editorderdate" id="editorderdate" required="">
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#1 Bread</label><br>
    		<input type="radio" name="editorderbread" id="editorderbread" value="Whole Wheat">Whole Wheat
    		<input type="radio" name="editorderbread" id="editorderbread" value="Italian Herb">Italian Herb
    		<input type="radio" name="editorderbread" id="editorderbread" value="Jalapeno Parmesan">Jalapeno Parmesan
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#2 Sauce</label><br>
    		<input type="radio" name="editordersauce" id="editordersauce" value="Mayo">Mayo
    		<input type="radio" name="editordersauce" id="editordersauce" value="Mustard">Mustard
    		<input type="radio" name="editordersauce" id="editordersauce" value="Honey Mustard">Honey Mustard
    		<input type="radio" name="editordersauce" id="editordersauce" value="Spicy Mayo">Spicy Mayo
    		<input type="radio" name="editordersauce" id="editordersauce" value="No Sauce">No Sauce
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#3 Sandwich Type</label><br>
    		<input type="radio" name="editordersandwich" id="editordersandwich" value="Turkey Bacon Club">Turkey Bacon Club
    		<input type="radio" name="editordersandwich" id="editordersandwich" value="Oven Roasted Turkey">Oven Roasted Turkey
    		<input type="radio" name="editordersandwich" id="editordersandwich" value="Savory Ham">Savory Ham
    		<input type="radio" name="editordersandwich" id="editordersandwich" value="Italian">Italian(Salami,Ham & Pepperoni)
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#4 Cheese</label><br>
    		<input type="radio" name="editordercheese" id="editordercheese" value="American">American
    		<input type="radio" name="editordercheese" id="editordercheese" value="Swiss">Swiss
    		<input type="radio" name="editordercheese" id="editordercheese" value="Pepper Jack">Pepper Jack
    		<input type="radio" name="editordercheese" id="editordercheese" value="No Cheese">No Cheese
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#5 Veggies</label><br>
    		<input type="radio" name="editorderveggies" id="editorderveggies" value="Cucumber">Cucumber
    		<input type="radio" name="editorderveggies" id="editorderveggies" value="Lettuce">Lettuce
    		<input type="radio" name="editorderveggies" id="editorderveggies" value="Peppers-Banana">Peppers-Banana
    		<input type="radio" name="editorderveggies" id="editorderveggies" value="Peppers-Jalapeno">Peppers-Jalapeno
    		<input type="radio" name="editorderveggies" id="editorderveggies" value="Peppers-Green and Red">Peppers-Green and Red
    		<br>
    		<input type="radio" name="editorderveggies" id="editorderveggies" value="Pickles">Pickles
    		<input type="radio" name="editorderveggies" id="editorderveggies" value="Spinach">Spinach
    		<input type="radio" name="editorderveggies" id="editorderveggies" value="Tomato">Tomato
    		<input type="radio" name="editorderveggies" id="editorderveggies" value="Olives">Olives
    		<input type="radio" name="editorderveggies" id="editorderveggies" value="Onions">Onions
    		<input type="radio" name="editorderveggies" id="editorderveggies" value="No Veggies">No Veggies
    		<input type="hidden" name="orderid" id="orderid">
    	</div>
    	<p style="font-size: 20px;"><u>Order for 8:30am</u></p>
    	<input type="submit" name="submit" id="submit" onclick="order()" value="Edit Order">
    	
	</div>
  </div>
	<div class="editmodal-footer" id="editorderfooter">
    <h3>Modal Footer</h3>
    </form>
  	</div>
</div>
<!-- EDIT ORDER MODAL -->

<!-- DELETE ORDER MODAL -->
<div id="deletemodal" class="deletemodal">
	<div class="deletemodal-header">
    <span class="deleteclose">&times;</span>
    	<h2>Delete Order</h2>
  	</div>
  <!-- Modal content -->
  <div class="deletemodal-content">
    <span class="deleteclose">&times;</span>
    <p class="para" style="position: center; text-align: center; font-size: 30px">Late orders are not accepted</p>
    <p class="para" style="position: center; text-align: center; font-size: 20px; color: red;"><u>Orders not picked-up will not be allowed to pre-order again</u><br><img src="../images/sandwich.png" style="width: 100px; height: 50px;"></p>
    <div style="margin-left: 20%; margin-right: 20%;padding-bottom: 50px; align:center">

    <form method="POST" action="orderdelete.php" id="deleteorderform">
    <label>First and Last Name: </label><input type="text" readonly name="deleteordername" id="deleteordername" readonly><label class="errorordername" id="errorordername"></label>
    <label>Date: </label><input type="date" name="deleteorderdate" id="deleteorderdate" readonly="">
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#1 Bread</label><br>
    		<input type="radio" name="deleteorderbread" id="deleteorderbread" value="Whole Wheat" checked="Checked" disabled="">Whole Wheat
    		<input type="radio" name="deleteorderbread" id="deleteorderbread" value="Italian Herb" disabled="">Italian Herb
    		<input type="radio" name="deleteorderbread" id="deleteorderbread" value="Jalapeno Parmesan" disabled="">Jalapeno Parmesan
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#2 Sauce</label><br>
    		<input type="radio" name="deleteordersauce" id="deleteordersauce" value="Mayo" checked="Checked" disabled="">Mayo
    		<input type="radio" name="deleteordersauce" id="deleteordersauce" value="Mustard" disabled="">Mustard
    		<input type="radio" name="deleteordersauce" id="deleteordersauce" value="Honey Mustard" disabled="">Honey Mustard
    		<input type="radio" name="deleteordersauce" id="deleteordersauce" value="Spicy Mayo" disabled="">Spicy Mayo
    		<input type="radio" name="deleteordersauce" id="deleteordersauce" value="No Sauce" disabled="">No Sauce
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#3 Sandwich Type</label><br>
    		<input type="radio" name="deleteordersandwich" id="deleteordersandwich" value="Turkey Bacon Club" checked="Checked" disabled="">Turkey Bacon Club
    		<input type="radio" name="deleteordersandwich" id="deleteordersandwich" value="Oven Roasted Turkey" disabled="">Oven Roasted Turkey
    		<input type="radio" name="deleteordersandwich" id="deleteordersandwich" value="Savory Ham" disabled="">Savory Ham
    		<input type="radio" name="deleteordersandwich" id="deleteordersandwich" value="Italian" disabled="">Italian(Salami,Ham & Pepperoni)
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#4 Cheese</label><br>
    		<input type="radio" name="deleteordercheese" id="deleteordercheese" value="American" checked="Checked" disabled="">American
    		<input type="radio" name="deleteordercheese" id="deleteordercheese" value="Swiss" disabled="">Swiss
    		<input type="radio" name="deleteordercheese" id="deleteordercheese" value="Pepper Jack" disabled="">Pepper Jack
    		<input type="radio" name="deleteordercheese" id="deleteordercheese" value="No Cheese" disabled="">No Cheese
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#5 Veggies</label><br>
    		<input type="radio" name="deleteorderveggies" id="deleteorderveggies" value="Cucumber" checked="Checked" disabled="">Cucumber
    		<input type="radio" name="deleteorderveggies" id="deleteorderveggies" value="Lettuce" disabled="">Lettuce
    		<input type="radio" name="deleteorderveggies" id="deleteorderveggies" value="Peppers-Banana" disabled="">Peppers-Banana
    		<input type="radio" name="deleteorderveggies" id="deleteorderveggies" value="Peppers-Jalapeno" disabled=""Peppers-Jalapeno
    		<input type="radio" name="deleteorderveggies" id="deleteorderveggies" value="Peppers-Green and Red" disabled="">Peppers-Green and Red
    		<br>
    		<input type="radio" name="deleteorderveggies" id="deleteorderveggies" value="Pickles" disabled="">Pickles
    		<input type="radio" name="deleteorderveggies" id="deleteorderveggies" value="Spinach" disabled="">Spinach
    		<input type="radio" name="deleteorderveggies" id="deleteorderveggies" value="Tomato" disabled="">Tomato
    		<input type="radio" name="deleteorderveggies" id="deleteorderveggies" value="Olives" disabled="">Olives
    		<input type="radio" name="deleteorderveggies" id="deleteorderveggies" value="Onions" disabled="">Onions
    		<input type="radio" name="deleteorderveggies" id="deleteorderveggies" value="No Veggies" disabled="">No Veggies
    		<input type="hidden" name="deleteorderid" id="deleteorderid">
    	</div>
    	<p style="font-size: 20px;"><u>Order for 8:30am</u></p>
    	<input style="background-color: red" type="submit" name="submit" id="submit" onclick="order()" value="Delete Order">
    	
	</div>
  </div>
	<div class="deletemodal-footer" id="deleteorderfooter">
    <h3>Modal Footer</h3>
    </form>
  	</div>
</div>
<!-- DELETE ORDER MODAL -->
</body>
</html>
<script type="text/javascript">
var ordertable;
$(document).ready(function() {
	ordertable = $("#ordertable").DataTable({
		'ajax': 'orderFetch.php',
		'order': []
	});
});
// Get the modal
var modal = document.getElementById('addordermodal');
var editmodal = document.getElementById('editmodal');
var deletemodal = document.getElementById('deletemodal');

// Get the button that opens the modal
var btn = document.getElementById("addorderbtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
  if (event.target == editmodal) {
   	editmodal.style.display = "none";
  }
  if (event.target == deletemodal) {
    deletemodal.style.display = "none";
  }

}


function order(){

	
	$("#addorderform").unbind('submit').bind('submit', function() {
		var ordername= $("#ordername").val();
      	var orderdate = $("#orderdate").val();
      	var orderbread = $("#orderbread").val();
      	var ordersauce = $("#ordersauce").val();
      	var ordersandwich = $("#ordersandwich").val();
      	var ordercheese = $("#ordercheese").val();
      	var orderveggies = $("#orderveggies").val();

    	if (isNotEmtpty(ordername) && isNotEmtpty(orderdate) && orderbread && ordersauce && ordersandwich && ordercheese && orderveggies) 
    	{
    		$(".formcontent").hide();
        	$(".ajaxloader").show();
        	$.ajax({
        		url : $("#addorderform").attr('action'),
		        type: $("#addorderform").attr('method'),
		        data: $("#addorderform").serialize(),
		        dataType: 'json',
		        success:function(response)
		        {
		        }

        	})
    	}
	});
  function isNotEmpty(caller){
    if (caller.val() = '') {
      caller.css('border', '1px solid red');
      return false;
    }
    else
      caller.css('border','');
    return true;
  }
}
function orderedit(orderid = null)
{
	var editorderbtn = document.getElementById("editorderbtn");
	var editspan = document.getElementsByClassName("editclose")[0];
	editspan.onclick = function() {
 		editmodal.style.display = "none";
	}
	var editmodal = document.getElementById('editmodal');
	editmodal.style.display = "block";
	if (orderid) 
	{
		$("#editorderform")[0].reset();
		 // niload sa modal ung data
		$("#orderid").val(orderid);
		
		$.ajax({
				url: 'orderfetchselected.php',
						type: 'post',
						data: {orderid : orderid},
						dataType: 'json',
						success:function(response) 
						{
							
							$("#editordername").val(response.ordername); //niload sa modal ung data
							$("#editorderdate").val(response.orderdate);
							switch(response.orderbread)
							{
								case "Whole Wheat":
								var x = document.getElementsByName("editorderbread");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Whole Wheat") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Italian Herb":
								var x = document.getElementsByName("editorderbread");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Italian Herb") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Jalapeno Parmesan":
								var x = document.getElementsByName("editorderbread");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Jalapeno Parmesan") {
									      x[i].checked = true;
									    }
									  }
								 break;
								default:
								 break;
							}
							switch(response.ordersauce)
							{
								case "Mayo":
								var x = document.getElementsByName("editordersauce");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Mayo") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Mustard":
								var x = document.getElementsByName("editordersauce");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Mustard") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Honey Mustard":
								var x = document.getElementsByName("editordersauce");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Honey Mustard") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Spicy Mayo":
								var x = document.getElementsByName("editordersauce");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Spicy Mayo") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "No Sauce":
								var x = document.getElementsByName("editordersauce");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "No Sauce") {
									      x[i].checked = true;
									    }
									  }
								 break;
								default:
								 break;
							}
							switch(response.ordersandwich)
							{
								case "Turkey Bacon Club":
								var x = document.getElementsByName("editordersandwich");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Turkey Bacon Club") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Oven Roasted Turkey":
								var x = document.getElementsByName("editordersandwich");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Oven Roasted Turkey") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Savory Ham":
								var x = document.getElementsByName("editordersandwich");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Savory Ham") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Italian":
								var x = document.getElementsByName("editordersandwich");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Italian") {
									      x[i].checked = true;
									    }
									  }
								 break;
								default:
								 break;
							}
							switch(response.ordercheese)
							{
								case "American":
								var x = document.getElementsByName("editordercheese");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "American") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Swiss":
								var x = document.getElementsByName("editordercheese");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Swiss") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Pepper Jack":
								var x = document.getElementsByName("editordercheese");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Pepper Jack") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "No Cheese":
								var x = document.getElementsByName("editordercheese");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "No Cheese") {
									      x[i].checked = true;
									    }
									  }
								 break;
								default:
								 break;
							}
							switch(response.orderveggies)
							{
								case "Cucumber":
								var x = document.getElementsByName("editorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Cucumber") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Lettuce":
								var x = document.getElementsByName("editorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Lettuce") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Peppers-Banana":
								var x = document.getElementsByName("editorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Peppers-Banana") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Peppers-Jalapeno":
								var x = document.getElementsByName("editorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Peppers-Jalapeno") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Peppers-Green and Red":
								var x = document.getElementsByName("editorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Peppers-Green and Red") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Pickles":
								var x = document.getElementsByName("editorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Pickles") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Spinach":
								var x = document.getElementsByName("editorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Spinach") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Tomato":
								var x = document.getElementsByName("editorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Tomato") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Olives":
								var x = document.getElementsByName("editorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Olives") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Onions":
								var x = document.getElementsByName("editorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Onions") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "No Veggies":
								var x = document.getElementsByName("editorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "No Veggies") {
									      x[i].checked = true;
									    }
									  }
								 break;
								default:
								 break;
							}
							$("#editorderform").unbind('submit').bind('submit', function() {		

								var editordername = $("#editordername").val();
								var editorderdate = $("#editorderdate").val();
								var editorderbread = $("#editorderbread").val();
								var editordersauce = $("#editordersauce").val();
								var editordersandwich = $("#editordersandwich").val();
								var editordercheese = $("#editordercheese").val();
								var editorderveggies = $("#editorderveggies").val();
								
								if(editordername && editorderdate && editorderbread && editordersauce && editordersandwich && editordercheese && editorderveggies && orderid) 
								{
									
									var form = $(this);
									$.ajax({
										url : form.attr('action'),
										type: form.attr('method'),
										data: form.serialize(),
										dataType: 'json',
										success:function(response)
										{
												if(response.success == true) 
													{
															// reload the manage member table 
															ordertable.ajax.reload(null, false);						

									  	  					$("#editmodal").modal('hide');
															$("#editorderform")[0].reset();
									  	  			
												}  // if

												else if(response.success == false) 
												{
															ordertable.ajax.reload(null, false);						

									  	  					$("#editmodal").modal('hide');
															$("#editorderform")[0].reset();
								  	  			}  // elseif


										} // /success

									}); // /ajax	
									
								} // if
							});
							
						} // on success ng ajax na ituu

			}); // ajax nanaman ituu
	}
}

function orderdelete(orderid = null)
{
	var deleteorderbtn = document.getElementById("deleteorderbtn");
	var deletespan = document.getElementsByClassName("deleteclose")[0];
	deletespan.onclick = function() {
 		deletemodal.style.display = "none";
	}
	var deletemodal = document.getElementById('deletemodal');
	deletemodal.style.display = "block";
	if (orderid) 
	{
		$("#deleteorderform")[0].reset();
		 // niload sa modal ung data
		$("#deleteorderid").val(orderid);

		$.ajax({
				url: 'orderfetchselected.php',
						type: 'post',
						data: {orderid : orderid},
						dataType: 'json',
						success:function(response) 
						{
							
							$("#deleteordername").val(response.ordername); //niload sa modal ung data
							$("#deleteorderdate").val(response.orderdate);
							switch(response.orderbread)
							{
								case "Whole Wheat":
								var x = document.getElementsByName("deleteorderbread");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Whole Wheat") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Italian Herb":
								var x = document.getElementsByName("deleteorderbread");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Italian Herb") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Jalapeno Parmesan":
								var x = document.getElementsByName("deleteorderbread");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Jalapeno Parmesan") {
									      x[i].checked = true;
									    }
									  }
								 break;
								default:
								 break;
							}
							switch(response.ordersauce)
							{
								case "Mayo":
								var x = document.getElementsByName("deleteordersauce");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Mayo") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Mustard":
								var x = document.getElementsByName("deleteordersauce");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Mustard") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Honey Mustard":
								var x = document.getElementsByName("deleteordersauce");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Honey Mustard") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Spicy Mayo":
								var x = document.getElementsByName("deleteordersauce");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Spicy Mayo") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "No Sauce":
								var x = document.getElementsByName("deleteordersauce");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "No Sauce") {
									      x[i].checked = true;
									    }
									  }
								 break;
								default:
								 break;
							}
							switch(response.ordersandwich)
							{
								case "Turkey Bacon Club":
								var x = document.getElementsByName("deleteordersandwich");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Turkey Bacon Club") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Oven Roasted Turkey":
								var x = document.getElementsByName("deleteordersandwich");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Oven Roasted Turkey") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Savory Ham":
								var x = document.getElementsByName("deleteordersandwich");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Savory Ham") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Italian":
								var x = document.getElementsByName("deleteordersandwich");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Italian") {
									      x[i].checked = true;
									    }
									  }
								 break;
								default:
								 break;
							}
							switch(response.ordercheese)
							{
								case "American":
								var x = document.getElementsByName("deleteordercheese");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "American") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Swiss":
								var x = document.getElementsByName("deleteordercheese");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Swiss") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Pepper Jack":
								var x = document.getElementsByName("deleteordercheese");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Pepper Jack") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "No Cheese":
								var x = document.getElementsByName("deleteordercheese");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "No Cheese") {
									      x[i].checked = true;
									    }
									  }
								 break;
								default:
								 break;
							}
							switch(response.orderveggies)
							{
								case "Cucumber":
								var x = document.getElementsByName("deleteorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Cucumber") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Lettuce":
								var x = document.getElementsByName("deleteorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Lettuce") {
									      x[i].checked = true;
									    }
									  }
								 break;
								case "Peppers-Banana":
								var x = document.getElementsByName("deleteorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Peppers-Banana") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Peppers-Jalapeno":
								var x = document.getElementsByName("deleteorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Peppers-Jalapeno") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Peppers-Green and Red":
								var x = document.getElementsByName("deleteorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Peppers-Green and Red") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Pickles":
								var x = document.getElementsByName("deleteorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Pickles") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Spinach":
								var x = document.getElementsByName("deleteorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Spinach") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Tomato":
								var x = document.getElementsByName("deleteorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Tomato") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Olives":
								var x = document.getElementsByName("deleteorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Olives") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "Onions":
								var x = document.getElementsByName("deleteorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "Onions") {
									      x[i].checked = true;
									    }
									  }
								 break;
								 case "No Veggies":
								var x = document.getElementsByName("deleteorderveggies");
								var i;
								for (i = 0; i < x.length; i++) {
									    if (x[i].value == "No Veggies") {
									      x[i].checked = true;
									    }
									  }
								 break;
								default:
								 break;
							}
							$("#deleteorderform").unbind('submit').bind('submit', function() {		
								
								if(deleteordername && deleteorderdate && deleteorderbread && deleteordersauce && deleteordersandwich && deleteordercheese && deleteorderveggies && orderid) 
								{
									var form = $(this);
									$.ajax({
										url : form.attr('action'),
										type: form.attr('method'),
										data: form.serialize(),
										dataType: 'json',
										success:function(response)
										{
												if(response.success == true) 
													{
															// reload the manage member table 
															ordertable.ajax.reload(null, false);						

									  	  					$("#deletemodal").modal('hide');
															$("#deleteorderform")[0].reset();
									  	  			
												}  // if

												else if(response.success == false) 
												{
															ordertable.ajax.reload(null, false);						

									  	  					$("#deletemodal").modal('hide');
															$("#deleteorderform")[0].reset();
								  	  			}  // elseif


										} // /success

									}); // /ajax	
									
								} // if
							});
							
						} // on success ng ajax na ituu

			}); // ajax nanaman ituu
	}
}
</script>