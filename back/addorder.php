<?php 	
date_default_timezone_set("Asia/Manila");
require_once '../front/connect.php';

$valid['success'] = array(
	'success' => true,
	'messages' => array()
);

$ordername = $_POST['ordername'];
$orderdate = $_POST['orderdate'];
$orderbread = $_POST['orderbread'];
$ordersauce = $_POST['ordersauce'];
$ordersandwich = $_POST['ordersandwich'];
$ordercheese = $_POST['ordercheese'];
$orderveggies = $_POST['orderveggies'];
$datetimenow = date("Y-m-d H:i:s");


	$sql = "INSERT INTO `preorders`(`date_submitted`, `ordername`, `orderdate`, `orderbread`, `ordersauce`, `ordersandwich`, `ordercheese`, `orderveggies`) VALUES ('$datetimenow','$ordername','$orderdate','$orderbread','$ordersauce','$ordersandwich','$ordercheese','$orderveggies')";
	if($connect->query($sql) === TRUE) 
	{
		$valid['success'] = true;
		$valid['messages'] = "Order successfully Sent.";
		$connect->close();
		echo json_encode($valid);
		header("Location: index.php");
	}
	else
	{
		$valid['success'] = false;
		$valid['messages'] = "Network connection not stable. Please try again later.";
		$connect->close();
		echo json_encode($valid);	
	}

?>