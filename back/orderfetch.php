<?php

require_once '../front/connect.php';

$sql = "SELECT id, ordername, orderdate, orderbread, ordersauce, ordersandwich, ordercheese, orderveggies, orderstat FROM preorders WHERE orderstat < 404";
$result = $connect->query($sql);

$output = array('data' => array());

if($result->num_rows > 0) 
{
  while($row = $result->fetch_array())
  {
    $orderid = $row[0];
    
    $button = '<div><button class="editbtn" type="button" id="editorderbtn" data-toggle="modal" data-target="#editmodal" onclick="orderedit('.$orderid.')">EDIT</button><div>
    <div><button class="deletebtn" type="button" id="deleteorderbtn"  data-toggle="modal" data-target="#deletemodal" onclick="orderdelete('.$orderid.')">DELETE</button><div>';

    $output['data'][] = array( $row['ordername'], $row['orderdate'], $row['orderbread'], $row['ordersauce'], $row['ordersandwich'], $row['ordercheese'], $row['orderveggies'], $button );

  } // while
  
} //if

$connect->close();

echo json_encode($output);