-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2019 at 09:43 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `posub`
--

-- --------------------------------------------------------

--
-- Table structure for table `preorders`
--

CREATE TABLE `preorders` (
  `id` int(11) NOT NULL,
  `date_submitted` datetime NOT NULL,
  `ordername` varchar(255) NOT NULL,
  `orderdate` date NOT NULL,
  `orderbread` varchar(255) NOT NULL,
  `ordersauce` varchar(255) NOT NULL,
  `ordersandwich` varchar(255) NOT NULL,
  `ordercheese` varchar(255) NOT NULL,
  `orderveggies` varchar(255) NOT NULL,
  `orderstat` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `preorders`
--

INSERT INTO `preorders` (`id`, `date_submitted`, `ordername`, `orderdate`, `orderbread`, `ordersauce`, `ordersandwich`, `ordercheese`, `orderveggies`, `orderstat`) VALUES
(1, '2019-02-06 19:44:04', 'Anton Pogi po weh?sss', '2019-02-15', 'Whole Wheat', 'Honey Mustard', 'Savory Ham', 'No Cheese', 'No Veggies', 0),
(2, '2019-02-06 19:46:15', 'sasa', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 404),
(3, '2019-02-06 19:49:05', 'dasaddas', '2019-02-15', 'Italian Herb', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(4, '2019-02-06 19:49:52', 'asds', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 404),
(5, '2019-02-06 19:57:46', 's', '2019-02-07', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(6, '2019-02-06 19:58:17', 'asds', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(7, '2019-02-06 20:00:27', 'asds', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(8, '2019-02-06 20:01:01', 's', '2019-02-07', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(9, '2019-02-06 20:01:45', 's', '2019-02-07', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(10, '2019-02-06 20:02:27', 'asds', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(11, '2019-02-06 20:03:38', 'asds', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(12, '2019-02-06 20:03:46', 'asds', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(13, '2019-02-06 20:04:05', 'asds', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(14, '2019-02-06 20:04:14', 'asds', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(15, '2019-02-06 20:05:23', 'asds', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(16, '2019-02-06 20:05:36', 'asds', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(17, '2019-02-06 20:05:45', 'asds', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(18, '2019-02-06 20:06:04', 'asds', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(19, '2019-02-06 20:07:08', 'asds', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(20, '2019-02-06 20:07:56', 'asds', '2019-02-15', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0),
(21, '2019-02-06 22:34:05', 'Anton', '2019-02-13', 'Whole Wheat', 'Mayo', 'Turkey Bacon Club', 'American', 'Cucumber', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `preorders`
--
ALTER TABLE `preorders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `preorders`
--
ALTER TABLE `preorders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
