<?php 	
date_default_timezone_set("Asia/Manila");
require_once 'connect.php';

$valid['success'] = array(
	'success' => true,
	'messages' => array()
);

$ordername = $_POST['ordername'];
$orderdate = $_POST['orderdate'];
$orderbread = $_POST['orderbread'];
$ordersauce = $_POST['ordersauce'];
$ordersandwich = $_POST['ordersandwich'];
$ordercheese = $_POST['ordercheese'];
$orderveggies = $_POST['orderveggies'];
$datetimenow = date("Y-m-d H:i:s");


	$sql = "INSERT INTO `preorders`(`date_submitted`, `ordername`, `orderdate`, `orderbread`, `ordersauce`, `ordersandwich`, `ordercheese`, `orderveggies`) VALUES ('$datetimenow','$ordername','$orderdate','$orderbread','$ordersauce','$ordersandwich','$ordercheese','$orderveggies')";
	if($connect->query($sql) === TRUE) 
	{
		$valid['success'] = true;
		$valid['messages'] = "Order successfully Sent.";
		$connect->close();
		echo json_encode($valid);
	}
	else
	{
		$valid['success'] = false;
		$valid['messages'] = "Network connection not stable. Please try again later.";
		$connect->close();
		echo json_encode($valid);	
	}

?>
<!DOCTYPE html>
<html>
<head>
	<title>SUCCESS!!!</title>
	<style type="text/css">
		body {
  background-color: #222222;
  background: repeating-linear-gradient(45deg, #2b2b2b 0%, #2b2b2b 10%, #222222 0%, #222222 50%) 0 / 15px 15px;
}

	</style>
</head>
<body>
	<h1 style="align: center; font-size: 50px; text-align: center; color: white;">You've successfully sent your order!!</h1>
	<div align="center">
	<a href="index.php" style="background-color: #4CAF50;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;">Order Again</a>
  </div>
</body>
</html>