<!DOCTYPE html>
<html>
<head>
	<title>Pre-Order SUB</title>
	<style type="text/css">
		body {
  background-color: #222222;
  background: repeating-linear-gradient(45deg, #2b2b2b 0%, #2b2b2b 10%, #222222 0%, #222222 50%) 0 / 15px 15px;
}

#container {
  width: 500px;
  margin: auto;
}

/*Neon*/
p {
  text-align: center;
  font-size: 6em;
  margin: 20px 0 20px 0;
}

a {
  text-decoration: none;
  -webkit-transition: all 0.5s;
  -moz-transition: all 0.5s;
  transition: all 0.5s;
}

p:nth-child(1) a {
  color: #fff;
  font-family: Monoton;
  -webkit-animation: neon1 1.5s ease-in-out infinite alternate;
  -moz-animation: neon1 1.5s ease-in-out infinite alternate;
  animation: neon1 1.5s ease-in-out infinite alternate;
}

p:nth-child(1) a:hover {
  color: #FF1177;
  -webkit-animation: none;
  -moz-animation: none;
  animation: none;
}

p:nth-child(2) a {
  font-size: 1em;
  color: #228DFF;
  font-family: Iceland;
}

p:nth-child(2) a:hover {
  -webkit-animation: neon2 1.5s ease-in-out infinite alternate;
  -moz-animation: neon2 1.5s ease-in-out infinite alternate;
  animation: neon2 1.5s ease-in-out infinite alternate;
}



p a:hover {
  color: #ffffff;
}
/*glow for webkit*/

@-webkit-keyframes neon1 {
  from {
    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #FF1177, 0 0 70px #FF1177, 0 0 80px #FF1177, 0 0 100px #FF1177, 0 0 150px #FF1177;
  }
  to {
    text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #FF1177, 0 0 35px #FF1177, 0 0 40px #FF1177, 0 0 50px #FF1177, 0 0 75px #FF1177;
  }
}

@-webkit-keyframes neon2 {
  from {
    text-shadow: 0 0 10px #fff, 0 0 20px #fff, 0 0 30px #fff, 0 0 40px #228DFF, 0 0 70px #228DFF, 0 0 80px #228DFF, 0 0 100px #228DFF, 0 0 150px #228DFF;
  }
  to {
    text-shadow: 0 0 5px #fff, 0 0 10px #fff, 0 0 15px #fff, 0 0 20px #228DFF, 0 0 35px #228DFF, 0 0 40px #228DFF, 0 0 50px #228DFF, 0 0 75px #228DFF;
  }
}


/*REEEEEEEEEEESPONSIVE*/

@media (max-width: 650px) {
  #container {
    width: 100%;
  }
  p {
    font-size: 3.5em;
  }
}
/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  position: relative;
  background-color: #fefefe;
  margin: auto;
  padding: 0;
  border: 1px solid #888;
  width: 100%;
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
  -webkit-animation-name: animatetop;
  -webkit-animation-duration: 0.4s;
  animation-name: animatetop;
  animation-duration: 0.4s
}

/* Add Animation */
@-webkit-keyframes animatetop {
  from {top:-300px; opacity:0} 
  to {top:0; opacity:1}
}

@keyframes animatetop {
  from {top:-300px; opacity:0}
  to {top:0; opacity:1}
}

/* The Close Button */
.close {
  color: white;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}

.modal.open {
   display: flex;
   flex-direction: column;
}
.modal-header {
  padding: 2px 16px;
  background-color: #5cb85c;
  color: white;
}

.modal-body {padding: 2px 16px;}

.modal-footer {
  padding: 2px 16px;
  background-color: #5cb85c;
  color: white;
}
input[type=text], select {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit] {
  width: 100%;
  background-color: #4CAF50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=date]{
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
}

input[type=submit]:hover {
  background-color: #45a049;
}
/* width */
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}
	</style>
</head>
<body>
<div id="container">

  <p><a href="#">
    Pre-Order SUB
  </a></p>

  <p><a href="#" id="orderbtn" type="button" data-toggle="modal" data-target="#orderModal">CLICK ME!</a></p>


</div>

<div id="linkBack" style="position:absolute;right:0px;top:0px;background-color:#333;margin:0;width:60px;padding:5px"><a href="../back/index.php" style="font-size:14px;text-decoration:none;color:#fff;padding:0 0 0 0px;font-family:sans-serif; text-align: left">Admin Side</a></div>


<!-- ORDER MODAL -->
<div id="orderModal" class="modal">
	<div class="modal-header">
    <span class="close">&times;</span>
    	<h2>Pre-Order your SUB by 8:30am</h2>
  	</div>
  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
    <p class="para" style="position: center; text-align: center; font-size: 30px">Late orders are not accepted</p>
    <p class="para" style="position: center; text-align: center; font-size: 20px; color: red;"><u>Orders not picked-up will not be allowed to pre-order again</u><br><img src="../images/sandwich.png" style="width: 100px; height: 50px;"></p>
    <div style="margin-left: 20%; margin-right: 20%;padding-bottom: 50px; align:center">

    <form method="POST" action="order.php" id="orderform">
    <label>First and Last Name: </label><input type="text" name="ordername" id="ordername" required=""><label class="errorordername" id="errorordername"></label>
    <label>Date: </label><input type="date" name="orderdate" id="orderdate" required="">
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#1 Bread</label><br>
    		<input type="radio" name="orderbread" id="orderbread" value="Whole Wheat" checked="Checked">Whole Wheat
    		<input type="radio" name="orderbread" id="orderbread" value="Italian Herb">Italian Herb
    		<input type="radio" name="orderbread" id="orderbread" value="Jalapeno Parmesan">Jalapeno Parmesan
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#2 Sauce</label><br>
    		<input type="radio" name="ordersauce" id="ordersauce" value="Mayo" checked="Checked">Mayo
    		<input type="radio" name="ordersauce" id="ordersauce" value="Mustard">Mustard
    		<input type="radio" name="ordersauce" id="ordersauce" value="Honey Mustard">Honey Mustard
    		<input type="radio" name="ordersauce" id="ordersauce" value="Spicy Mayo">Spicy Mayo
    		<input type="radio" name="ordersauce" id="ordersauce" value="No Sauce">No Sauce
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#3 Sandwich Type</label><br>
    		<input type="radio" name="ordersandwich" id="ordersandwich" value="Turkey Bacon Club" checked="Checked">Turkey Bacon Club
    		<input type="radio" name="ordersandwich" id="ordersandwich" value="Oven Roasted Turkey">Oven Roasted Turkey
    		<input type="radio" name="ordersandwich" id="ordersandwich" value="Savory Ham">Savory Ham
    		<input type="radio" name="ordersandwich" id="ordersandwich" value="Italian">Italian(Salami,Ham & Pepperoni)
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#4 Cheese</label><br>
    		<input type="radio" name="ordercheese" id="ordercheese" value="American" checked="Checked">American
    		<input type="radio" name="ordercheese" id="ordercheese" value="Swiss">Swiss
    		<input type="radio" name="ordercheese" id="ordercheese" value="Pepper Jack">Pepper Jack
    		<input type="radio" name="ordercheese" id="ordercheese" value="No Cheese">No Cheese
    	</div>
    	<div style="border: 1px solid black; border-radius: 25px; margin: 5px; padding: 10px;">
    		<label>#5 Veggies</label><br>
    		<input type="radio" name="orderveggies" id="orderveggies" value="Cucumber" checked="Checked">Cucumber
    		<input type="radio" name="orderveggies" id="orderveggies" value="Lettuce">Lettuce
    		<input type="radio" name="orderveggies" id="orderveggies" value="Peppers-Banana">Peppers-Banana
    		<input type="radio" name="orderveggies" id="orderveggies" value="Peppers-Jalapeno">Peppers-Jalapeno
    		<input type="radio" name="orderveggies" id="orderveggies" value="Peppers-Green and Red">Peppers-Green and Red
    		<br>
    		<input type="radio" name="orderveggies" id="orderveggies" value="Pickles">Pickles
    		<input type="radio" name="orderveggies" id="orderveggies" value="Spinach">Spinach
    		<input type="radio" name="orderveggies" id="orderveggies" value="Tomato">Tomato
    		<input type="radio" name="orderveggies" id="orderveggies" value="Olives">Olives
    		<input type="radio" name="orderveggies" id="orderveggies" value="Onions">Onions
    		<input type="radio" name="orderveggies" id="orderveggies" value="No Veggies">No Veggies
    	</div>
    	<p style="font-size: 20px;"><u>Order for 8:30am</u></p>
    	<input type="submit" name="submit" id="submit" onclick="order()" value="Submit Order">
    	</form>
	</div>
  </div>
	<div class="modal-footer">
    <h3>Modal Footer</h3>
  	</div>
</div>
<!-- ORDER MODAL -->

</body>
</html>
<script type="text/javascript">
// Get the modal
var modal = document.getElementById('orderModal');

// Get the button that opens the modal
var btn = document.getElementById("orderbtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
  modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}


function order(){
	$("#orderform").unbind('submit').bind('submit', function() {
		var ordername= $("#ordername").val();
      	var orderdate = $("#orderdate").val();
      	var orderbread = $("#orderbread").val();
      	var ordersauce = $("#ordersauce").val();
      	var ordersandwich = $("#ordersandwich").val();
      	var ordercheese = $("#ordercheese").val();
      	var orderveggies = $("#orderveggies").val();

    	if (isNotEmtpty(ordername) && isNotEmtpty(orderdate) && orderbread && ordersauce && ordersandwich && ordercheese && orderveggies) 
    	{
    		$(".formcontent").hide();
        	$(".ajaxloader").show();
        	$.ajax({
        		url : $("#orderform").attr('action'),
		        type: $("#orderform").attr('method'),
		        data: $("#orderform").serialize(),
		        dataType: 'json',
		        success:function(response)
		        {
		        }

        	})
    	}
	});
  function isNotEmpty(caller){
    if (caller.val() = '') {
      caller.css('border', '1px solid red');
      return false;
    }
    else
      caller.css('border','');
    return true;
  }
}

</script>