# Purple Bug - Pre-order SUB

----- Reason for not using Git------
Haven't been too familiar with using Git because I only use the old ways like compressing the folder 
and adding the date and time in the rar file.
--------------------------------------------------

--------Pre-order SUB-------------------
The main page is on "front" folder index.php, you can start from there. :)
The SQL file is on the SQL folder import the SQL to use the system.
Import first the database, the SQL file is in the SQL folder...


---------Ordering--------------------------
In the main page you can place and order by clicking the button "Click me!". 
There you can see a modal pop-up in your screen for the form.

After placing your order, you'll be sent to a page for success which is order.php.
In that page you can see a button which is  "Order Again!", by clicking that you'll be sent to the main page.
------------------------------------------------

---------Admin CRUD------------------
in the main page you can see a button in the top right corner, by clicking that you'll be sent to the back-end
part. (I know you need login in going to the admin side, I can do it but in the requirements there's no such
thing as account module for the admin side. And it'll consume my 6 hour allotment for this project :D)

In the back-end main page you can see the table for the orders.
The Data table has pagination and search input box for ease of access.

1. ADD Order
In the top left part of the container there is a green "Add Order" button which will pop-up a modal for the
form that you'll fill up in placing an order. After submitting the table will reload and you can see your order in the last row.

2. EDIT Order
In the Option column of the table, every row has an Edit button for you can edit an order.
After clicking the specific edit button of a row a modal will pop-up for the form(autofilled from the database)
that you want to edit. After submitting the form the table will reload,

3. DELETE Order
In the Option column of the table, every row has an Delete button for you can delete an order.
After clicking the specific delete button of a row a modal will pop-up for the form(autofilled from the database)
that you want to delete. After submitting the form the the status of the order will just change and will not be
delete from the database.(I use that kind of process so there is a chance for Archives)
The table will reload.
---------------------------------------------------

THANK YOU FOR READING
Hope you'll consider my application, i'll be working hard to learn from you.